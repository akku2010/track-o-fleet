import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the MsgUtilityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-msg-utility',
  templateUrl: 'msg-utility.html',
})
export class MsgUtilityPage {
  islogin: any;
  obj: any=[];
  username: any;
  testList: any;
  filterObj: any=[];
  isTrade: boolean = false
  isNonTrade: boolean = false
  checkAllNonTrades: boolean = false
  checkAllTrades: boolean = false

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController) {
      this.islogin = JSON.parse(localStorage.getItem('details')) || {};
      if (navParams.get("param") != null) {
        this.obj = this.navParams.get("param");
        this.filterObj = JSON.parse(JSON.stringify(this.obj));


        // console.log("cheack obj", this.filterObj);
        // console.log("length", this.obj.length)
        // this.username = this.obj[i].first_name
        // console.log("chek username", this.username);
      }
      if (localStorage.getItem("AlreadyDimissed") !== null) {
        localStorage.removeItem("AlreadyDimissed");
      }
      if (localStorage.getItem('AlreadyClicked') !== null) {
        localStorage.removeItem("AlreadyClicked");
      }
      // const searchbar = document.querySelector('ion-searchbar');
      // const items = Array.from(document.querySelector('ion-list').children);

      // searchbar.addEventListener('ionInput', handleInput);

      // function handleInput(event) {
      //   const query = event.target.value.toLowerCase();
      //   requestAnimationFrame(() => {
      //     items.forEach(item => {
      //       const shouldShow = item.textContent.toLowerCase().indexOf(query) > -1;
      //        item.style.display = shouldShow ? 'block' : 'none';
      //     });
      //   });
      // }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MsgUtilityPage');
  }

  selectedArray :any = [];

checkAll(){
  debugger
  console.log("inside")
  console.log("chlist", this.selectedArray);
  //this.back();
  this.dismiss();
}

// getItems(ev: any) {
//   // Reset items back to all of the items
//   //this.initializeItems();

//   // set val to the value of the searchbar
//   const val = ev.target.value;

//   // if the value is an empty string don't filter the items
//   if (val && val.trim() != '') {
//     this.obj = this.obj.filter((item) => {
//       return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
//     })
//   }
// }

getItems (event) {
console.log(event.target.value);
  // set val to the value of the searchbar
  const val = event.target.value;
  debugger
  // if the value is an empty string don't filter the items
  if (val && val.trim() != '') {

    this.filterObj = this.obj.filter((item) => {
      console.log("search result=> " + JSON.stringify(item))
      return (item.first_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      //return (item.toString().toLowerCase().indexOf(val.toString().toLowerCase()) > -1);

    })
    // this.filterObj = []
    // for(var i=0;i<this.obj.length; i++){
    //     if(this.obj[i].first_name.toLowerCase() ==  val.toLowerCase()) {
    //       this.filterObj.push(this.obj[i])
    //     }

    // }

  }else{
    this.filterObj = this.obj;
  }
  console.log("onj", this.obj);
}


back() {
  this.navCtrl.push("FastagListPage", {
    "selecteduser": this.selectedArray
  })
}

// read(event) {
//   const checked = event.target.checked;
//   this.trade.forEach(item => item.selected = checked);
//   console.log("inside the function");
//   this.filterObj = this.obj;
//   console.log("chk", this.filterObj);
// }

// clear() {
//   console.log("inside the function");
//   this.filterObj = [];
//   console.log("chk", this.filterObj)
// }

dismiss() {
  debugger
  let data = this.selectedArray;
  this.viewCtrl.dismiss(data);
  // this.viewCtrl.dismiss({
  //   "selecteduser": d
  // });
// console.log("chf", d)
}

  // selectMember(data){
  //   debugger
  //   this.selectedArray = [];
  //   if (data.length > 0) {
  //     if (data.length > 1) {
  //       for (var t = 0; t < data.length; t++) {
  //         this.selectedArray.push(data[t]._id)
  //         console.log("list", this.selectedArray)
  //       }
  //     } else {
  //       this.selectedArray.push(data[0]._id)
  //     }
  //   } else return;
  //   console.log("selectedVehicle=> ", this.selectedArray)
  //  }

  selectMember(data){
    debugger
    if (data.checked == true) {
       this.selectedArray.push(data);
     } else {
      let newArray = this.selectedArray.filter(function(el) {
        return el.phone !== data.phone;
     });
      this.selectedArray = newArray;
    }
    console.log("st",this.selectedArray);
   }






}
